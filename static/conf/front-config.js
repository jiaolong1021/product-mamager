var FRONT_CONFIG = {
  project: {
    title: '中铝股份生产管控系统',
    name: '中铝股份生产管控系统',
    logo: '/static/images/logo.png'
  },
  keycloakConfig: {
    rootUrl: 'http://iamserver-pc.uimpdev.prd.uimpcloud.com/', // 用户中心后台地址
    keycloak: {
      init: {
        onLoad: 'login-required',
        checkLoginIframe: false
      },
      config: {
        url: 'http://iamauth-pc.uimpdev.prd.uimpcloud.com/auth/', // keycloak管理地址
        clientId: 'product-manager', // 客户端
        realm: 'business' // 领域
      }
    }
  },
  apiServer: {
    WS_URL: 'ws://iamserver-pc.uimpdev.prd.uimpcloud.com',
    IAM_URL: 'http://iamserver.uimpdev.prd.uimpcloud.com/',
    BASE_URL: 'http://gateway.chalco-pcs.prd.uimpcloud.com/workbench', // 后端服务地址(工作台)
    KEYCLOAK_URL: 'http://iamserver-pc.uimpdev.prd.uimpcloud.com', // 用户中心后台地址
    SYS_URL: 'http://gateway.chalco-pcs.prd.uimpcloud.com/content', // 系统管理后台地址
    MESSAGE_CENTER: 'http://uimp-msg-center.pttest.prd.uimpcloud.com',
    CARD_URL: 'http://gateway.chalco-pcs.prd.uimpcloud.com/workbench',
    INDEX: 'http://gateway.chalco-pcs.prd.uimpcloud.com/index'

  },
  mock: true,
  mockData: {
    url: {
      'product-manager-sys': 'http://localhost:9581/',
      'chalco-pcs-workbench': 'http://localhost:9581/',
      '/planManage': 'http://192.168.3.66:9583/',
      'product-index': 'http://localhost:9582/'
    }
  }
}
