var FRONT_CONFIG = {
  project: {
    title: '生产管控',
    name: '生产管控',
    logo: '/static/images/logo.png'
  },
  keycloakConfig: {
    rootUrl: 'http://iamserver.scgk.zyzx.chalco.cn/', // 用户中心后台地址
    keycloak: {
      init: {
        onLoad: 'login-required',
        checkLoginIframe: false
      },
      config: {
        url: 'http://auth.scgk.zyzx.chalco.cn/auth/', // keycloak管理地址
        clientId: 'product-manager', // 客户端
        realm: 'business' // 领域
      }
    }
  },
  apiServer: {
    WS_URL: 'ws://iamserver.scgk.zyzx.chalco.cn',
    IAM_URL: 'http://iam.scgk.zyzx.chalco.cn/',
    BASE_URL: 'http://gateway.scgk.uimp.zyzx.chalco.cn/workbench', // 后端服务地址(质量)
    // BASE_URL: 'http://192.168.18.142:4002',
    KEYCLOAK_URL: 'http://iamserver.scgk.zyzx.chalco.cn/', // 用户中心后台地址
    SYS_URL: 'http://pcs-baseinfo.zy.prd.uimpcloud.co', // 系统管理后台地址
    MOCK_URL: 'http://yapi.cs2025.cn/mock/342/fcg',
    PES_URL: 'http://uimp-personnel-center.product-line.prd.uimpcloud.com', // 人员中心后台
    INDEX: 'http://gateway.scgk.uimp.zyzx.chalco.cn/index',
    CARD_URL: 'http://gateway.scgk.uimp.zyzx.chalco.cn/workbench', // 工作台后端
    MESSAGE_CENTER: 'http://api.msg.scgk.zyzx.chalco.cn' // 后端服务地址
  },
  mock: true,
  mockData: {
    url: {
      'product-manager-sys': 'http://localhost:9581/',
      'chalco-pcs-workbench': 'http://localhost:9581/',
      '/planManage': 'http://192.168.3.66:9583/',
      'product-index': 'http://localhost:9582/'
    }
  }
}
