import TreeSelectWrapper from './tree-select-wrapper'
import Organizations from './organizations'
import Dictionary from './dictionary'
import OrgSearchUserModal from './org-search-user-modal'
import AddressInput from './address-input'
import AsyncSelect from './async-select'
import TreeSelectOrg from './tree-select-org'
import zlstLayout from '@/components/layout'
import calculator from './calculator-model'

import '@riophae/vue-treeselect/dist/vue-treeselect.css'

export const ZlstTreeSelectWrapper = TreeSelectWrapper
export const ZlstOrganizations = Organizations
export const ZlstDictionary = Dictionary
export const ZlstOrgSearchUserModal = OrgSearchUserModal
export const ZlstAddressInput = AddressInput
export const ZlstAsyncSelect = AsyncSelect
export const ZlstTreeSelectOrg = TreeSelectOrg
export const ZlstEleLayout = zlstLayout
export const ZlstCalculator = calculator

const install = function(Vue) {
  Vue.component(ZlstTreeSelectWrapper.name, ZlstTreeSelectWrapper)
  Vue.component(ZlstOrganizations.name, ZlstOrganizations)
  Vue.component(ZlstDictionary.name, ZlstDictionary)
  Vue.component(ZlstOrgSearchUserModal.name, ZlstOrgSearchUserModal)
  Vue.component(ZlstAddressInput.name, ZlstAddressInput)
  Vue.component(ZlstAsyncSelect.name, ZlstAsyncSelect)
  Vue.component(ZlstTreeSelectOrg.name, ZlstTreeSelectOrg)
  Vue.component(ZlstEleLayout.name, ZlstEleLayout)
  Vue.component(ZlstCalculator.name, ZlstCalculator)
}

export default {
  install,
  ZlstOrganizations,
  ZlstDictionary,
  ZlstAddressInput,
  ZlstTreeSelectOrg,
  ZlstEleLayout,
  ZlstCalculator
}
