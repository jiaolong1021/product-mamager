import ZlstCalculator from './src/index.vue'

ZlstCalculator.install = function(Vue) {
  Vue.component(ZlstCalculator.name, ZlstCalculator)
}

export default ZlstCalculator
