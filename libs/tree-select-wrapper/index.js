import ZlstTreeSelectWrapper from './src/tree-select-wrapper.vue'

ZlstTreeSelectWrapper.install = function(Vue) {
  Vue.component(ZlstTreeSelectWrapper.name, ZlstTreeSelectWrapper)
}

export default ZlstTreeSelectWrapper
