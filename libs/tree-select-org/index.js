import ZlstTreeSelectOrg from './src/tree-select-org.vue'

ZlstTreeSelectOrg.install = function(Vue) {
  Vue.component(ZlstTreeSelectOrg.name, ZlstTreeSelectOrg)
}

export default ZlstTreeSelectOrg
