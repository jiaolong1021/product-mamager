import ZlstAsyncSelect from './src/async-select.vue'

ZlstAsyncSelect.install = function(Vue) {
  Vue.component(ZlstAsyncSelect.name, ZlstAsyncSelect)
}

export default ZlstAsyncSelect
