import ZlstAddressInput from './src/address-input.vue'

ZlstAddressInput.install = function(Vue) {
  Vue.component(ZlstAddressInput.name, ZlstAddressInput)
}

export default ZlstAddressInput
