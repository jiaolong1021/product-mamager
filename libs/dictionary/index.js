import ZlstDictionary from './src/dictionary.vue'

ZlstDictionary.install = function(Vue) {
  Vue.component(ZlstDictionary.name, ZlstDictionary)
}

export default ZlstDictionary
