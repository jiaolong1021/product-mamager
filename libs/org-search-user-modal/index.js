import ZlstOrgSearchUserModal from './src/org-search-user-modal.vue'

ZlstOrgSearchUserModal.install = function(Vue) {
  Vue.component(ZlstOrgSearchUserModal.name, ZlstOrgSearchUserModal)
}

export default ZlstOrgSearchUserModal
