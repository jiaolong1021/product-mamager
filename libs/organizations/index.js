import ZlstOrganizations from './src/organizations.vue'

ZlstOrganizations.install = function(Vue) {
  Vue.component(ZlstOrganizations.name, ZlstOrganizations)
}

export default ZlstOrganizations
