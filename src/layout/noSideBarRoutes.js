// 部分收缩侧边栏
export const noSidebarList = [
  "/comprehensiveSearch/comprehensiveSearchMain",
  "/personalInformation/personalInformationMain",
  "/manager/systemManager/serviceAnnouncement/articleList",
  "/manager/systemManager/helpDocument/helpDocumentShow",
  "/manager/systemManager/productExpress/articleList"
];
// 完全收缩侧边栏
export const noAnySidebar = [
  "/index/workBench/board",
  "/allMessage/allMessageMain",
  "/readMessage/readMessageMain",
  "/UnreadMessage/UnreadMessageMain",
  "/404",
  "/overviewManage",
  "/overviewManage/view",
  "/overviewManage/electrolysisSummaryOne",
  "/overviewManage/electrolysisSummaryTwo",
  "/overviewManage/dailyInfo",
  "/overviewManage/aluminaProduce",
  "/personalInformation/personalInformationMain",
  "/manager/systemManager/serviceAnnouncement/detail", // 服务公告详情
  "/manager/systemManager/productExpress/detail", // 产品快报详情
  "/manager/systemManager/helpDocument/detail" // 帮助文档详情
];
