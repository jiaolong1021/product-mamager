import Vue from 'vue'

import ZlstSidebar from './zlstSidebar/index'
import keycloak from './keycloak'
import ZlstHeader from '@/layout/zlstHeader'

const components = [
  ZlstSidebar,
  ZlstHeader
]

const install = function(Vue, opts = {}, lang) {
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

Vue.prototype.$keycloak = keycloak

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  version: '1.0.6',
  install: install,
  ZlstSidebar,
  ZlstHeader
}
