import Vue from 'vue'
import VueKeycloakJs from '@dsb-norge/vue-keycloak-js'
import axios from 'axios'
const DEFAULT_CONFIG = {
  keycloak: {
    init: {
      onLoad: 'login-required',
      checkLoginIframe: false
    },
    config: {
      url: 'http://keycloak.cs2025.com/auth/',
      clientId: 'zlst-wl',
      realm: 'business'
    }
  }
}
export default (options) => {
  options = options || {}
  return new Promise((resolve, reject) => {
    let init = DEFAULT_CONFIG.keycloak.init
    let config = DEFAULT_CONFIG.keycloak.config
    if (options && options.keycloak) {
      if (options.keycloak.config) {
        config = Object.assign({}, config, options.keycloak.config)
      }
      if (options.keycloak.init) {
        init = Object.assign({}, init, options.keycloak.init)
      }
    }
    Vue.use(VueKeycloakJs, {
      init,
      config,
      onReady: kc => {
        const rootUrl = options.rootUrl || ''
        Vue.prototype.$zlst = {
          rootUrl
        }
        axios.get(`${rootUrl}v3/users/info`, {
          params: {
            realm: kc.realm,
            username: kc.idTokenParsed.preferred_username
          },
          headers: {
            Authorization: 'Bearer ' + kc.token,
            realm: kc.realm,
            clientId: kc.clientId
          }
        }).then((res) => {
          const userInfo = {
            token: kc.token,
            realm: kc.realm,
            clientId: kc.clientId,
            ...res.data.data
          }
          Vue.prototype.$userInfo = userInfo
          resolve(userInfo)
        }).catch((err) => {
          console.error(err)
          reject(err)
        })
      }
    })
  })
}
