export const whiteList = [
  "/UnreadMessage",
  "/UnreadMessage/UnreadMessageMain",
  "/readMessage",
  "/readMessage/readMessageMain",
  "/personalInformation",
  "/personalInformation/personalInformationMain",
  "/allMessage",
  "/allMessage/allMessageMain",
  "/comprehensiveSearch",
  "/comprehensiveSearch/comprehensiveSearchMain"
]; // no redirect whitelist
