// import { MessageBox, Message } from 'element-ui'
import { Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import fetch from '@zlst/fetch'
import vue from 'vue'
const Fetch = fetch({
  message: Message,
  baseURL: FRONT_CONFIG.apiServer.BASE_URL,
  timeout: 60000
})
// request interceptor
Fetch.interceptors.request.use(
  config => {
    if (vue.prototype.$userInfo) {
      config.headers['Authorization'] = 'Bearer ' + vue.prototype.$userInfo.token
      config.headers['realm'] = vue.prototype.$userInfo.realm
      config.headers['clientId'] = vue.prototype.$userInfo.clientId
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

export default Fetch
