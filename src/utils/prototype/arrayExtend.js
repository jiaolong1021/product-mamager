/**
 * Created by huangbiao on 2017/6/1
 */
export default {
  init: function() {
    /** **************** Array 对象扩展***********************/
    /**
         * @exports Array
         * Array 对象扩展
         * @type {{removeRepeat: Function, del: Function, indexOf: Function, contains: Function}}
         */
    var ArrayPrototypeExtend = {
      /**
             * 去除数组中的重复项
             * @returns {Array} 返回几个新的数组
             */
      removeRepeat: function() {
        var reset = []; var done = {}
        for (var i = 0; i < this.length; i++) {
          var temp = this[i]
          // 这里的json对象一定要以数组的方式访问，否则会认为找不到这个对象
          if (!done[temp]) {
            done[temp] = true
            reset.push(temp)
          }
        }
        return reset
      },

      /**
             * 删除指定的数组
             * @param n
             * @returns {*}
             */
      del: function(n) {
        if (n < 0) return this
        return this.slice(0, n).concat(this.slice(n + 1, this.length))
      },

      /**
             * 克隆当前数组
             * @returns {Array.<T>}
             */
      clone: function() {
        return [].concat(this)
      },
      /**
             * 数组第一次出现指定元素值的位置
             * @param o
             * @returns {number}
             */
      indexOf: function(o) {
        for (var i = 0; i < this.length; i++) if (this[i] === o) return i
        return -1
      },

      /**
             *  检索数组元素（原型扩展或重载）
             * @param {o} 被检索的元素值
             * @type int
             * @returns 元素索引
             */
      contains: function(o) {
        var index = -1
        for (var i = 0; i < this.length; i++) {
          if (this[i] === o) {
            index = i
            break
          }
        }
        return index
      }
    }
    // Array.prototype = $.extend(true, ArrayPrototypeExtend, Array.prototype);

    for (var key in ArrayPrototypeExtend) {
      Array.prototype[key] = ArrayPrototypeExtend[key]
    }
  }
}
