/**
 * Created by huangbiao on 2017/6/1
 */
// 扩展数组对象
import arrayExtendObj from './arrayExtend.js'
// 扩展日期对象
import dateExtendObj from './dateExtend.js'
// 扩展字符串对象
import stringExtendObj from './stringExtend.js'

(function extendDefaultObject() {
  arrayExtendObj.init()
  dateExtendObj.init()
  stringExtendObj.init()
})()

export default {
  init: function() {

  }
}
