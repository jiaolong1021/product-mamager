/**
 * Created by huangbiao on 2017/6/1
 */
export default {
  init: function() {
    /** **************** String 对象扩展***********************/

    /**
         * @exports Sring
         * String 对象扩展
         * @type {{htmlEncode: (*|Function), toJson: Function}}
         */
    var StringPrototypeExtend = {
      /**
             * 检查是否是URL地址
             */
      isURL: function() {
        // is.js 原码 https://github.com/arasatasaygin/is.js
        var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i
        var result = re.test(this)
        return result
      },

      /**
             * 检查表单是否是email字符串
             */
      isEmail: function() {
        // is.js 原码 https://github.com/arasatasaygin/is.js
        var re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
        var result = re.test(this)
        return result
      },

      /**
             * 检查号码，正确格式为:XXXX-XXXXXXX，XXXX-XXXXXXXX，XXX-XXXXXXX，XXX-XXXXXXXX，XXXXXXX，XXXXXXXX
             */
      isPhoneNumber: function() {
        var re = /^((\d{3,4})|\d{3,4}-)?\d{7,8}$/
        var result = re.test(this)
        return result
      },
      /**
             * 获取字符串长度（原型扩展或重载）
             * @type int
             * @returns 字符串长度
             */
      len: function() {
        var arr = this.match(/[^\x00-\xff]/ig)
        return this.length + (arr == null ? 0 : arr.length)
      },

      /**
             * //检查是否是浮点数，包括0，包括正浮点数  负浮点数
             * @returns {boolean}
             */
      isFloat: function() {
        var re = /^-?([1-9]\d*.\d+|0.\d*[1-9]\d*|0?.0+|0)$/
        var result = re.test(this)
        return result
      },

      /**
             * 检查是否是负数
             * @returns {boolean}
             */
      isNegativ: function() {
        if (isNaN(this)) {
          return false
          throw Error('It is not a Number')
        }
        // return this < 0;
        var re = /^-(([1-9][0-9]*)$|([1-9]\d*.\d+|0.\d*[1-9]\d*))$/
        // var re=/^-{1}([1-9][0-9]*)$;
        var result = re.test(this)
        return result
      },
      /**
             * 检查是否是正数
             * @returns {boolean}
             */
      isPositive: function() {
        if (isNaN(this)) {
          return false
          throw Error('It is not a Number')
        }
        // return this >= 0;
        var re = /(^[1-9][0-9]*)$|([1-9]\d*.\d+|0.\d*[1-9]\d*)$/
        var result = re.test(this)
        return result && this >= 0
      },
      /**
             * //检查是否是非负浮点数，包括0
             * @param isContainZero
             * @returns {boolean}
             */
      isFloatNegativ: function(isContainZero) {
        var re, result
        if (isContainZero) {
          re = /(^-([1-9]\d*.\d+|0.\d*[1-9]\d*)$)|(0)/
          result = re.test(this)
        } else {
          re = /^-([1-9]\d*.\d+|0.\d*[1-9]\d*)$/
          result = re.test(this)
        }
        return result
      },
      /**
             * //检查是否是正浮点数，包括0
             * @param isContainZero
             * @returns {boolean}
             */
      isFloatPositive: function(isContainZero) {
        var re, result
        if (isContainZero) {
          re = /^[1-9]\d*.\d+|0.\d*[1-9]\d*|0?.0+|0$/
          result = re.test(this)
        } else {
          re = /^[1-9]\d*.\d+|0.\d*[1-9]\d*$/
          result = re.test(this)
        }
        return result
      },
      /**
             * //检查是否是整数，包括0，正整数，负整数
             * @returns {boolean}
             */
      isInt: function() {
        var re = /^-?\d+$/
        var result = re.test(this)
        return result
      },

      /**
             * 检查是否是正整数
             * isContainZero 是否包含0
             */
      isIntPositive: function(isContainZero) {
        var re, result
        if (isContainZero) {
          re = /(^[1-9]*[1-9][0-9]*$)|(0)/
          result = re.test(this)
        } else {
          re = /^[1-9]*[1-9][0-9]*$/
          result = re.test(this)
        }
        return result
      },
      /**
             * 检查是否是负整数
             * @param isContainZero 是否包含0
             * @returns {boolean}
             */
      isIntNegativ: function(isContainZero) {
        var re, result
        if (isContainZero) {
          re = /(^-[1-9]*[1-9][0-9]*$)|(0)/
          result = re.test(this)
        } else {
          re = /^-[1-9]*[1-9][0-9]*$/
          result = re.test(this)
        }
        return result
      },
      /**
             * 检查字符串是否是指定字符串开头，返回true 和 false
             * @param str
             * @returns {boolean}
             */
      isStartWith: function(str) {
        var reg = new RegExp('^' + str)
        return reg.test(this)
      },
      /**
             * 检查字符串是否是指定字符串结尾，返回true 和 false
             * @param str
             * @returns {boolean}
             */
      isEndWith: function(str) {
        var reg = new RegExp(str + '$')
        return reg.test(this)
      },
      /**
             * 一个单词首字母大写,返回字符串
             * @returns {string}
             */
      capitalize: function() {
        var result = this.charAt(0).toUpperCase() + this.substring(1).toLocaleLowerCase()
        return result
      },
      /**
             * 保留字母和空格,返回字符串
             * @returns {string}
             */
      getEn: function() {
        var result = this.replace(/[^A-Z a-z]/g, '')
        return result
      },
      /**
             * 逆序
             * @returns {string}
             */
      reverse: function() {
        return this.split('').reverse().join('')
      },
      /**
             * 检查字符串是否包含自定字符串，返回true 和 false
             * @param target
             * @returns {boolean}
             */
      isContains: function(target) {
        var myReg = new RegExp(target)
        var result = myReg.test(this)
        return result
      },
      /**
             * 去除两边的空格,返回字符串
             * @returns {string}
             */
      trim: function() {
        var result = this.replace(/^\s+|\s+$/g, '')
        return result
      },
      /**
             * 除去左边空白,返回字符串
             * @returns {string}
             */
      trimLeft: function() {
        return this.replace(/^\s+/g, '')
      },
      /**
             * 除去右边空白,返回字符串
             * @returns {string}
             */
      trimRight: function() {
        return this.replace(/\s+$/g, '')
      },
      /**
             * 去除所有的空白
             * @returns {*}
             */
      delBlank: function() {
        var result = this.replace(/\s+/g, '')
        return result
      },

      /**
             * 字符串转换为日期型（原型扩展或重载）
             * @returns {Date} 日期
             */
      toDate: function() {
        var converted = Date.parse(this)
        var myDate = new Date(converted)
        if (isNaN(myDate)) {
          var arys = this.split('-')
          myDate = new Date(arys[0], --arys[1], arys[2])
        }
        return myDate
      },

      /**
             * HTML转义字符
             * @returns {string}
             * @example
             * var userInput = "我要点击<a href='https://www.baidu.com'>百度链接</a>测试<span style='color:red'>我是红色的字</span>";
             * console.log(userInput.htmlEncode());
             */
      htmlEncode: function() {
        var s = ''
        if (!this) return s

        if (this.length === 0) return ''
        s = this.replace(/&/g, '&amp;')
        s = s.replace(/</g, '&lt;')
        s = s.replace(/>/g, '&gt;')
        s = s.replace(/ /g, '&nbsp;')
        s = s.replace(/\'/g, '&#39;')
        s = s.replace(/\"/g, '&quot;')
        return s
      },

      /**
             * 删除字符串中HTML标签
             * @returns {*}
             * @example
             * var userInput = "我要点击<a href='https://www.baidu.com'>百度链接</a>测试<span style='color:red'>我是红色的字</span>";
             * console.log(userInput.removeHtmlTag());
             */
      removeHtmlTag: function() {
        var s = this.replace(/<\/?[^>]*>/g, '')
        return s
      },
      /**
             * 将json字符串转为json对象
             * @returns {*}
             */
      toJson: function() {
        return (new Function('return ' + this))()
      },

      /**
             * 检查用户输入的文字只能是中文，英文，数字，如果非则字符则替换空
             */
      normalCharInputOnly: function() {
        if (this.length >= 1) {
          return this.replace(/[^a-zA-Z0-9\u4e00-\u9fa5]/g, '')
        }
        return ''
      },
      /**
             * 检查是否是中文，英文，数字，如果是返回false，否则返回true
             */
      isNormalChar: function() {
        var re = /[^a-zA-Z0-9\u4e00-\u9fa5]/g
        var result = re.test(this)
        return result
      },

      /**
             * 检查是否含有utf16字符
             * @returns 如果是返回false，否则返回true
             */
      isUtf16: function() {
        var re = /[\ud800-\udbff][\udc00-\udfff]/g
        var result = re.test(this)
        return result
      },

      /**
             * 用于把用utf16编码的字符转换成实体字符，以供后台存储
             * @param  {string} 将要转换的字符串，其中含有utf16字符将被自动检出
             * @return {string} 转换后的字符串，utf16字符将被转换成&#xxxx;形式的实体字符
             */
      getEntitiesByUtf16: function() {
        var patt = /[\ud800-\udbff][\udc00-\udfff]/g // 检测utf16字符正则
        var result = ''
        result = this.replace(patt, function(char) {
          var H, L, code
          if (char.length === 2) {
            H = char.charCodeAt(0) // 取出高位
            L = char.charCodeAt(1) // 取出低位
            // 转换算法
            code = (H - 0xD800) * 0x400 + 0x10000 + L - 0xDC00
            return '&#' + code + ';'
          } else {
            return char
          }
        })
        return result
      },

      /**
             * 将URL地址转为restful风格
             * @param param
             * @returns {module:Sring}
             * @Example
             var url = "http://localhost:8080/yunpan/{username}/aaa/{name}";
             console.log( urlFormat(url, {username: '111', name: 'yc'}))
             */
      restfulFormat: function(param) {
        let that = this
        const keys = Object.keys(param)
        if (param === undefined || param === null || keys.length === 0) {
          return that
        }
        for (const key of keys) {
          that = that.replace(new RegExp('\\{' + key + '\\}', 'g'), param[key])
        }
        return that
      },
      /**
             * 将日期格式的字符串转为起始时间
             * @example
             var startDate="2016-09-21 2:1:2";
             var result = startDate.dateStart();
             * */
      dateStart: function() {
        return this.substr(0, 10) + ' 00:00:00'
      },
      /**
             * 将日期格式的字符串转为结束时间
             * @example
             var startDate="2016-09-21 2:1:2";
             var result = startDate.dateEnd();
             * */
      dateEnd: function() {
        return this.substr(0, 10) + ' 23:59:59'
      },
      /**
             * 验证是否为身份证号
             */
      isIdentity: function() {
        // var re = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        // var result = re.test(this);
        // return result;
        var city = {
          11: '北京',
          12: '天津',
          13: '河北',
          14: '山西',
          15: '内蒙古',
          21: '辽宁',
          22: '吉林',
          23: '黑龙江',
          31: '上海',
          32: '江苏',
          33: '浙江',
          34: '安徽',
          35: '福建',
          36: '江西',
          37: '山东',
          41: '河南',
          42: '湖北',
          43: '湖南',
          44: '广东',
          45: '广西',
          46: '海南',
          50: '重庆',
          51: '四川',
          52: '贵州',
          53: '云南',
          54: '西藏',
          61: '陕西',
          62: '甘肃',
          63: '青海',
          64: '宁夏',
          65: '新疆',
          71: '台湾',
          81: '香港',
          82: '澳门',
          91: '国外'
        }
        // 位数校验
        if (!/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|[xX])$/.test(this)) {
          return false
        }
        // 地区校验
        if (!city[this.substr(0, 2)]) {
          return false
        }
        // 出生日期
        var idBirthday = ''
        var idBirthdayMatch = null
        if (this.length === 15) {
          idBirthday = '19' + this.substr(6, 6)
        } else {
          idBirthday = this.substr(6, 8)
        }
        idBirthdayMatch = idBirthday.match(/^(\d{1,4})(\d{1,2})(\d{1,2})$/)
        if (idBirthdayMatch === null) {
          return false
        }
        var d = new Date(idBirthdayMatch[1], idBirthdayMatch[2] - 1, idBirthdayMatch[3])
        if (d.getFullYear() !== Number(idBirthdayMatch[1]) || (d.getMonth() + 1) !== Number(idBirthdayMatch[2]) || d.getDate() !== Number(idBirthdayMatch[3])) {
          return false
        }

        // 18位身份证需要验证最后一位校验位
        if (this.length === 18) {
          var code = this.split('')
          // ∑(ai×Wi)(mod 11)
          // 加权因子
          var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
          // 校验位
          var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2]
          var sum = 0
          var ai = 0
          var wi = 0
          for (var i = 0; i < 17; i++) {
            ai = code[i]
            wi = factor[i]
            sum += ai * wi
          }
          if (String(parity[sum % 11]) !== code[17].toUpperCase()) {
            return false
          }
        }
        return true
      },
      /**
             * 验证是否是手机号码
             */
      isMobilePhone: function() {
        // var re = /^1[3|4|5|7|8]\d{9}$/;
        var re = /^\d{11}$/
        var result = re.test(this)
        return result
      },
      isPhone: function() {
        var re = /(^1[3|4|5|7|8|]\d{9}$)|(^((\d{3,4})|\d{3,4}-)?\d{7,8}$)/
        var result = re.test(this)
        return result
      },
      /**
             * 是否银行卡号
             */
      isBankCard: function() {
        var re = /^\d{16}$|^\d{19}$/
        var result = re.test(this)
        return result
      },
      /**
             * 是否车牌号
             */
      isAutocar: function() {
        // 新能源汽车车牌：
        // 小型车标准：粤B F(D)12345
        var energySmallReg = /[\u4e00-\u9fa5][A-Z]([DF](?![a-zA-Z0-9]*[IO])[0-9]{5})/
        // 大型车标准：粤B 12345F(D)
        var energyBigReg = /[\u4e00-\u9fa5][A-Z](?![a-zA-Z0-9]*[IO])[0-9]{5}[DF]/
        // 普通车：
        // 粤A 45D5F(挂/警/澳/港/学)
        var commonReg = /^[冀豫云辽黑湘皖鲁苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼渝京津沪新京军空海北沈兰济南广成使领A-Z]{1}[a-zA-Z0-9]{5}[a-zA-Z0-9挂学警港澳]{1}$/
        var result = energySmallReg.test(this) || energyBigReg.test(this) || commonReg.test(this)
        return result
      },
      isPostCode: function() {
        var re = /^[0-9]{6}$/
        var result = re.test(this)
        return result
      },
      isNNS: function() {
        // var re =/^((13[0-9])|(17[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$/;
        var re = /^[1-9]{6}[A-Z1-9]{9}/
        var result = re.test(this)
        return result
      }
    }
    for (var key in StringPrototypeExtend) {
      String.prototype[key] = StringPrototypeExtend[key]
    }
  }
}
