/**
 * Created by huangbiao on 2017/6/1
 */
export default {
  init: function() {
    /** **************** Date 对象扩展***********************/

    /**
         * @exports Date
         * Date 对象扩展
         * @type {{getWeek: Function, addSeconds: Function, addMinutes: Function, addHours: Function, addDays: Function, addWeeks: Function, addMonths: Function, addYears: Function, format: (*|Function), dateDiff: Function, dateAdd: Function, getBeforeDate: Function, getAfterDate: Function, datePart: Function, toArray: Function, isLeapYear: Function}}
         */
    var DatePrototypeExtend = {
      /**
             * 获取星期
             * @returns {*}
             */
      getWeek: function() {
        var a = ['日', '一', '二', '三', '四', '五', '六']
        var week = new Date().getDay()
        var str = a[week]
        return str
      },

      /**
             * 将指定的秒数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addSeconds: function(value) {
        var second = this.getSeconds()
        this.setSeconds(second + value)
        return this
      },

      /**
             * 将指定的分钟数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addMinutes: function(value) {
        var minute = this.getMinutes()
        this.setMinutes(minute + value)
        return this
      },

      /**
             * 将指定的小时数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addHours: function(value) {
        var hour = this.getHours()
        this.setHours(hour + value)
        return this
      },

      /**
             * 将指定的天数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addDays: function(value) {
        var date = this.getDate()
        this.setDate(date + value)
        return this
      },

      /**
             * 将指定的星期数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addWeeks: function(value) {
        return this.addDays(value * 7)
      },

      /**
             * 将指定的月份数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addMonths: function(value) {
        var month = this.getMonth()
        this.setMonth(month + value)
        return this
      },

      /**
             * 将指定的年份数加到此实例的值上
             * @param value
             * @returns {Date}
             */
      addYears: function(value) {
        var year = this.getFullYear()
        this.setFullYear(year + value)
        return this
      },

      /**
             * 日期格式化（原型扩展或重载）
             * 格式 YYYY/yyyy/YY/yy 表示年份
             * MM/M 月份
             * W/w 星期
             * dd/DD/d/D 日期
             * hh/HH/h/H 时间
             * mm/m 分钟
             * ss/SS/s/S 秒
             * @param {formatStr} 格式模版
             * @type string
             * @returns 日期字符串
             */
      format: Date.prototype.format || function(formatStr) {
        var str = formatStr
        var Week = ['日', '一', '二', '三', '四', '五', '六']
        str = str.replace(/yyyy|YYYY/, this.getFullYear())
        str = str.replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
        str = str.replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1))
        str = str.replace(/M/g, this.getMonth() + 1)
        str = str.replace(/w|W/g, Week[this.getDay()])
        str = str.replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate())
        str = str.replace(/d|D/g, this.getDate())
        str = str.replace(/hh|HH/, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours())
        str = str.replace(/h|H/g, this.getHours())
        str = str.replace(/mm/, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes())
        str = str.replace(/m/g, this.getMinutes())
        str = str.replace(/ss|SS/, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds())
        str = str.replace(/s|S/g, this.getSeconds())
        return str
      },

      /**
             * 比较日期差（原型扩展或重载）
             * @param {strInterval} 日期类型:'y、m、d、h、n、s、w'
             * @param {dtEnd} 格式为日期型或者 有效日期格式字符串
             * @type int
             * @returns 比较结果
             */
      dateDiff: function(strInterval, dtEnd) {
        var dtStart = this
        if (typeof dtEnd === 'string') { // 如果是字符串转换为日期型
          dtEnd = toDate(dtEnd)
        }
        switch (strInterval) {
          case 's' :
            return parseInt((dtEnd - dtStart) / 1000)
          case 'n' :
            return parseInt((dtEnd - dtStart) / 60000)
          case 'h' :
            return parseInt((dtEnd - dtStart) / 3600000)
          case 'd' :
            return parseInt((dtEnd - dtStart) / 86400000)
          case 'w' :
            return parseInt((dtEnd - dtStart) / (86400000 * 7))
          case 'm' :
            return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1)
          case 'y' :
            return dtEnd.getFullYear() - dtStart.getFullYear()
        }
      },

      /**
             * 日期计算（原型扩展或重载）
             * @param {strInterval} 日期类型:'y、m、d、h、n、s、w'
             * @param {Number} 数量
             * @type Date
             * @returns 计算后的日期
             */
      dateAdd: function(strInterval, Number) {
        var dtTmp = this
        switch (strInterval) {
          case 's' :
            return new Date(Date.parse(dtTmp) + (1000 * Number))
          case 'n' :
            return new Date(Date.parse(dtTmp) + (60000 * Number))
          case 'h' :
            return new Date(Date.parse(dtTmp) + (3600000 * Number))
          case 'd' :
            return new Date(Date.parse(dtTmp) + (86400000 * Number))
          case 'w' :
            return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number))
          case 'q' :
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds())
          case 'm' :
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds())
          case 'y' :
            return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds())
        }
      },

      /**
             * daysNumber 表示当前日期往前推的天数
             * return Array
             * @param number
             */
      getBeforeDate: function(daysNumber) {
        var myDate = this
        var dateArray = []
        for (var i = 1; i <= daysNumber; i++) {
          var tempDate = new Date(myDate.getTime() - i * 24 * 3600 * 1000)
          // dateArray.push(tempDate.format('yyyy/MM/dd'));
          dateArray.push(tempDate)
        }
        return dateArray
      },

      /**
             * daysNumber 表示当前日期往后推的天数
             * return Array
             * @param number
             */
      getAfterDate: function(daysNumber) {
        var myDate = this
        var dateArray = []
        for (var i = 1; i <= daysNumber; i++) {
          var tempDate = new Date(myDate.getTime() + i * 24 * 3600 * 1000)
          // dateArray.push(tempDate.format('yyyy/MM/dd'));
          dateArray.push(tempDate)
        }
        return dateArray
      },

      /**
             * 取得日期数据信息（原型扩展或重载）
             * @param {interval} 日期类型:'y、m、d、h、n、s、w'
             * @type int
             * @returns 指定的日期部分
             */
      datePart: function(interval) {
        var myDate = this
        var partStr = ''
        var Week = ['日', '一', '二', '三', '四', '五', '六']
        switch (interval) {
          case 'y' :
            partStr = myDate.getFullYear()
            break
          case 'm' :
            partStr = myDate.getMonth() + 1
            break
          case 'd' :
            partStr = myDate.getDate()
            break
          case 'w' :
            partStr = Week[myDate.getDay()]
            break
          case 'ww' :
            partStr = myDate.WeekNumOfYear()
            break
          case 'h' :
            partStr = myDate.getHours()
            break
          case 'n' :
            partStr = myDate.getMinutes()
            break
          case 's' :
            partStr = myDate.getSeconds()
            break
        }
        return partStr
      },

      /**
             * 把日期分割成数组（原型扩展或重载）
             * @type array
             * @returns 日期数组
             */
      toArray: function() {
        var myDate = this
        var myArray = []
        myArray[0] = myDate.getFullYear()
        myArray[1] = myDate.getMonth() + 1
        myArray[2] = myDate.getDate()
        myArray[3] = myDate.getHours()
        myArray[4] = myDate.getMinutes()
        myArray[5] = myDate.getSeconds()
        return myArray
      },

      /**
             * 判断闰年（原型扩展或重载）
             * @type boolean
             * @returns 是否为闰年 true/false
             */
      isLeapYear: function() {
        return (this.getYear() % 4 === 0 && ((this.getYear() % 100 !== 0) || (this.getYear() % 400 === 0)))
      },

      /**
             *
             * */
      dateStart: function() {
        return this.format('yyyy-MM-dd') + ' 00:00:00'
      },
      dateEnd: function() {
        return this.format('yyyy-MM-dd') + ' 23:59:59'
      }

    }
    // Date.prototype = $.extend(true, DatePrototypeExtend, Date.prototype);
    for (var key in DatePrototypeExtend) {
      Date.prototype[key] = DatePrototypeExtend[key]
    }
  }
}
