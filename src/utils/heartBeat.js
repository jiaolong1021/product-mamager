import Vue from 'vue'

let ws = ''
let url = ''
export function createWebSocket(wsUrl, ctx) {
  try {
    url = wsUrl
    ws = new WebSocket(wsUrl)
    init(wsUrl, ctx)
    if (ctx) {
      ctx.ws = ws
    }
  } catch (e) {
    console.log('catch', e)
    reconnect(wsUrl)
  }
}

export function close() {
  ws.close && ws.close()
  ws = ''
}

function init(wsUrl, ctx) {
  ws.onclose = function(e) {
    console.log('链接关闭', e.code)
  }
  ws.onerror = function() {
    console.log('发生异常了')
    reconnect(wsUrl)
  }
  ws.onopen = function() {
    // 心跳检测重置
    heartCheck.start()
  }
  ws.onmessage = function(res) {
    console.log('接收到消息')
    // 拿到任何消息都说明当前连接是正常的
    heartCheck.start()
    if (res.data) {
      try {
        const msg = JSON.parse(res.data)
        if (typeof msg === 'object' && msg.code === 401) {
          // 关闭websocket，提示并退出登录
          // console.log(msg.message)
          Vue.prototype.$alert(msg.message, '提示', {
            confirmButtonText: '确定',
            callback: action => {
              localStorage.removeItem('vuex')
              Vue.prototype.$keycloak.logoutFn()
            }
          })
        }
      } catch (e) {
        console.log(e)
      }
    }
  }
}

let lockReconnect = false // 避免重复连接
let tt = ''
function reconnect(url) {
  if (lockReconnect) {
    return
  }
  lockReconnect = true
  // 没连接上会一直重连，设置延迟避免请求过多
  tt && clearTimeout(tt)
  tt = setTimeout(function() {
    createWebSocket(url)
    lockReconnect = false
  }, 4000)
}

const heartCheck = {
  timeout: 3000, // 每隔三秒发送心跳
  num: 3, // 3次心跳均未响应重连
  timeoutObj: null,
  start: function() {
    const _this = this
    let _num = this.num
    this.timeoutObj && clearTimeout(this.timeoutObj)
    this.timeoutObj = setTimeout(function() {
      // 这里发送一个心跳，后端收到后，返回一个心跳消息，
      // onmessage拿到返回的心跳就说明连接正常
      // ws.send('wsCode') // 心跳包
      _num--
      // 计算答复的超时次数
      if (_num === 0) {
        ws.colse()
        reconnect(url)
      }
    }, _this.timeout)
  }
}
