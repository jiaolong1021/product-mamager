import Vue from 'vue'
import { merge } from 'lodash'
import {
  Autocomplete,
  Loading,
  Avatar,
  Image,
  Table,
  TableColumn,
  Button,
  ButtonGroup,
  Message,
  Cascader,
  Pagination,
  Select,
  Option,
  Drawer,
  Input,
  Dialog,
  Form,
  Steps,
  Step,
  Timeline,
  TimelineItem,
  FormItem,
  // Breadcrumb,
  // BreadcrumbItem,
  Badge,
  Menu,
  Submenu,
  MenuItem,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Scrollbar,
  Tabs,
  TabPane,
  Upload,
  Tree,
  TimePicker,
  Row,
  Col,
  Divider,
  MessageBox,
  Tag,
  Tooltip,
  Radio,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  RadioButton,
  RadioGroup,
  DatePicker,
  Switch,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Card,
  InputNumber,
  Popover,
  Alert
} from 'element-ui'
Badge
Vue.component(Badge.name, Badge)

Vue.component(Autocomplete.name, Autocomplete)
Vue.component(Avatar.name, Avatar)
Vue.component(Image.name, Image)
Vue.component(Tag.name, Tag)
Vue.component(Tooltip.name, Tooltip)
Vue.component(DatePicker.name, DatePicker)
Vue.component(Upload.name, Upload)
Vue.component(Tree.name, Tree)
Vue.component(TimePicker.name, TimePicker)
Vue.component(Row.name, Row)
Vue.component(Col.name, Col)
Vue.component(Steps.name, Steps)
Vue.component(Step.name, Step)
Vue.component(Timeline.name, Timeline)
Vue.component(TimelineItem.name, TimelineItem)
Vue.component(Checkbox.name, Checkbox)
Vue.component(CheckboxButton.name, CheckboxButton)
Vue.component(CheckboxGroup.name, CheckboxGroup)
Vue.component(Radio.name, Radio)
Vue.component(RadioButton.name, RadioButton)
Vue.component(RadioGroup.name, RadioGroup)
Vue.component(Divider.name, Divider)
Vue.component(Container.name, Container)
Vue.component(Header.name, Header)
Vue.component(Aside.name, Aside)
Vue.component(Main.name, Main)
Vue.component(Footer.name, Footer)
Vue.component(Table.name, merge(Table, { // 使用组件定义的方式，对组件里的属性重新进行配置
  props: {
    stripe: {
      default: true
    },
    border: {
      default: true
    }
  }
}))
Vue.component(TableColumn.name, merge(TableColumn, { // 使用组件定义的方式，对组件里的属性重新进行配置
  props: {
    align: {
      default: 'center'
    }
  }
}))
Vue.component(Button.name, Button)
Vue.component(Cascader.name, Cascader)
Vue.component(ButtonGroup.name, ButtonGroup)
Vue.component(Cascader.name, Cascader)
Vue.component(Pagination.name, Pagination)
Vue.component(Select.name, Select)
Vue.component(Option.name, Option)
Vue.component(Drawer.name, Drawer)
Vue.component(Input.name, Input)
Vue.component(Dialog.name, Dialog)
Vue.component(Form.name, Form)
Vue.component(FormItem.name, FormItem)
Vue.component(Scrollbar.name, Scrollbar)
Vue.component(Tabs.name, Tabs)
Vue.component(TabPane.name, TabPane)
// Vue.component(Breadcrumb.name, Breadcrumb)
// Vue.component(BreadcrumbItem.name, BreadcrumbItem)
Vue.component(Menu.name, Menu)
Vue.component(Submenu.name, Submenu)
Vue.component(MenuItem.name, MenuItem)
Vue.component(InputNumber.name, InputNumber)

Vue.component(Dropdown.name, Dropdown)
Vue.component(DropdownMenu.name, DropdownMenu)
Vue.component(DropdownItem.name, DropdownItem)
// Vue.component(Switch.name, Switch)
// Vue.component(Card.name, Card)
Vue.component(Popover.name, Popover)
// Vue.component(Alert.name, Alert)
Vue.use(Loading.directive)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$alert = MessageBox.alert
