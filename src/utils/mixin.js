import { getAllOrganization } from './../api/server/user.js'
export default {
  methods: {
    async getAllOrgList(filterOrgId, hasAll) {
      const res = await getAllOrganization()
      let ret = [{
        id: '',
        name: '全部'
      }]
      if (filterOrgId) {
        res.data.forEach(org => {
          if (org.id === filterOrgId) {
            ret.push(org)
          }
        })
      } else {
        ret = ret.concat(res.data)
      }
      this.handleOrgList(ret)
      return ret
    },
    handleOrgList(orgList) {
      orgList.forEach(org => {
        if (org.child && org.child.length > 0) {
          org.children = org.child
          this.handleOrgList(org.children)
        }
        org.label = org.name
      })
    }
  }
}
