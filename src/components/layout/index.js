import ZlstEleLayout from './src/container.vue'
ZlstEleLayout.install = function(Vue) {
  Vue.component(ZlstEleLayout.name, ZlstEleLayout)
}
export default ZlstEleLayout
