import { getPolicy } from '@/api/password'
export default {
  data() {
    return {
      pwdPolicy: {},
      pwdCheck: {
        length: false, // 长度是否符合规范
        character: false, // 字符是否符合规范
        name: false, // 名称是否符合规范
        safe: 0 // 安全系数，最高3
      },
      randomPwd: ''
    }
  },
  mounted() {
    this.getPwdReg()
  },
  methods: {
    // 获取密码策略
    async getPwdReg() {
      if (this.psdReg.regEx) {
        this.pwdPolicy = { ...this.psdReg }
      } else {
        this.pwdPolicy = (await getPolicy(this.$userInfo.realm)).data || {}
      }
      console.log(this.pwdPolicy)
      if (this.pwdPolicy.regEx) {
        // 去掉长度匹配
        const str = this.pwdPolicy.regEx.split(`{${this.pwdPolicy.length},`)
        this.pwdPolicy.valideReg = str[0] + '*$'
        // console.log(res.data.regEx, this.pwdPolicy.valideReg)
      }
    },
    pwdChange(val, name) {
      // console.log(val)
      this.popoverVis = true
      this.pwdCheck.length = val.length >= this.pwdPolicy.length
      // const nameReverse = name.split('').reverse().join('')
      // this.pwdCheck.name = val !== name && val !== nameReverse
      const reg = new RegExp(this.pwdPolicy.valideReg)
      this.pwdCheck.character = reg.test(val)
      this.pwdCheck.safe = 0
      if (val.length > 0) {
        this.pwdCheck.safe++
      }
      if (val.length > 15) {
        this.pwdCheck.safe++
      }
      if (this.pwdCheck.character) {
        this.pwdCheck.safe++
      }
    }
  }
}
