export default {
  computed: {
    listRealmData() {
      return this.$userInfo.realm
    }
  },
  watch: {
    listRealmData(val) {
      this.$router.push({ path: '/clients/list', query: {
        t: +new Date() // 保证每次点击路由的query项都是不一样的，确保会重新刷新view
      }})
    }
  }
}
