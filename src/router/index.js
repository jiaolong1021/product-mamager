/**
 * @description 这部分页面主要负责的逻辑就是生成注册表/路由表，并动态生成路由，返回vRouter实例
 */
/* Layout */
import Vue from 'vue'
import Router from 'vue-router'
import PlainWrapper from '@/components/plainWrapper'

import store from '../store'

Vue.use(Router)

// 这里不再先定义router，而是将router实例导出动态加载
export const constantRoutes = [
  // 需要先定义好/，不然输入/会跳404，并且无法addRoutes
  {
    path: '/',
    redirect: store.state.sys.dashBoardRule
  },
  {
    path: '/personalInformation',
    name: 'personalInformation',
    redirect: '/personalInformation/personalInformationMain',
    component: PlainWrapper,
    meta: { title: '个人信息' },
    children: [
      {
        path: 'personalInformationMain',
        name: 'personalInformationMain',
        component: () => import('@/views/personalInformation/list'),
        meta: { title: '个人信息' }
      }
    ]
  },
  {
    path: '/UnreadMessage',
    name: 'UnreadMessage',
    redirect: '/UnreadMessage/UnreadMessageMain',
    component: PlainWrapper,
    meta: { title: '消息接收' },
    children: [
      {
        path: 'UnreadMessageMain',
        name: 'UnreadMessageMain',
        component: () => import('@/views/unread-message/list'),
        meta: { title: '未读消息' }
      }
    ]
  },
  {
    path: '/readMessage',
    name: 'readMessage',
    redirect: '/readMessage/readMessageMain',
    component: PlainWrapper,
    meta: { title: '消息接收' },
    children: [
      {
        path: 'readMessageMain',
        name: 'readMessageMain',
        component: () => import('@/views/read-message/list'),
        meta: { title: '已读消息' }
      }
    ]
  },
  {
    path: '/allMessage',
    name: 'allMessage',
    redirect: '/allMessage/allMessageMain',
    component: PlainWrapper,
    meta: { title: '消息接收' },
    children: [
      {
        path: 'allMessageMain',
        name: 'allMessageMain',
        component: () => import('@/views/all-message/list'),
        meta: { title: '全部消息' }
      }
    ]
  },
  {
    path: '/comprehensiveSearch',
    name: 'comprehensiveSearch',
    redirect: '/comprehensiveSearch/comprehensiveSearchMain',
    component: PlainWrapper,
    meta: { title: '综合搜索' },
    children: [
      {
        path: 'comprehensiveSearchMain',
        name: 'comprehensiveSearchMain',
        component: () => import('@/views/comprehensiveSearch/list'),
        meta: { title: '综合搜索' }
      }
    ]
  },

  {
    path: '/404',
    hidden: true,
    component: () => import('@/views/404/index.vue')
  },
  {
    path: '/report',
    hidden: true,
    component: () => import('@/views/report/report.vue')
  }
]
const pathList = []
const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
constantRoutes.map(v => {
  traverse(v, pathList, '')
})

function traverse(target, arr, parentPath) {
  let path = target.path
  if (path && !path.startsWith('/')) {
    path = '/' + target.path
  }
  if (path.endsWith('/')) {
    path = path.substring(0, path.length - 1)
  }
  path && arr.push(parentPath + path)
  if (target.children && target.children.length > 0) {
    target.children.map(v => {
      traverse(v, arr, parentPath + path)
    })
  }
}
store.commit('sys/SET_PATHS', pathList)

/**
 * 生成路由方法
 * @description 在不改变iam的结构和接口情况下，子应用标题为icon，子应用路由base为remark
 */
export function generateRealRoutes(routes) {
  // 只需要处理第一层即子应用注册层的数据
  constantRoutes[0].children = routes.map(v => {
    return {
      path: v.resUrl,
      meta: {
        title: v.name
      }
    }
  })
  constantRoutes[0].redirect = routes[0].resUrl
  console.log(constantRoutes, 'cons')
  const newRouter = new Router({
    routes: constantRoutes
  })
  router.matcher = newRouter.matcher
}

export default router
