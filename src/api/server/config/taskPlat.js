export default (configuration) => {
  const BASE_URL = configuration.apiServer.BASE_URL
  return {
    getTaskTable: BASE_URL + '/task-info/queryPage', // 插叙表格
    getAppList: BASE_URL + '/application-info/queryAppInfoList', // 查询所有应用
    getTypeList: BASE_URL + '/task-type/queryTaskTypePage', // 查询任务类型
    getPolarData: BASE_URL + '/task-statistics/queryTaskWaveChart', // 查询雷达图数据
    getPipeData: BASE_URL + '/task-statistics/queryTaskPieDiagram', // 查询饼图数据
    recentProds: BASE_URL + '/using-log',
    viewOrder: BASE_URL + '/views-info/updateViewSortNumber', // 排序
    getViewList: BASE_URL + '/views-info/queryViewsInfoList', // 获取所有视图
    saveView: BASE_URL + '/views-info/saveViewsInfo', // 新建视图
    editView: BASE_URL + '/views-info/updateViewsInfo', // 修改视图
    deleteView: BASE_URL + '/views-info/deleteViewsInfo', // 删除视图
    getTasks: BASE_URL + '/application-info/queryAppInfo'
  }
}
