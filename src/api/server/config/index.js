import { configuration } from '@/store/global'
import taskPlat from './taskPlat'
import personnelManager from './personnelManager'
import message from './massageCenter'
export const API_LIST = {
  ...taskPlat(configuration.baseUrl),
  ...personnelManager(configuration.baseUrl),
  ...message(configuration.baseUrl)

}
