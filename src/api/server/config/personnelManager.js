export default (configuration) => {
  return {
    dataDictionary: configuration.apiServer.BASEINFO_URL + '/dict',
    getDictItem: configuration.apiServer.BASEINFO_URL + '/dict-item',
    FieldqueryPage: configuration.apiServer.PES_URL + '/personnel-field/queryPage', // 人员信息扩展分页查询
    Fieldqueryadd: configuration.apiServer.PES_URL + '/personnel-field/add', // 人员信息扩展新增
    Fieldqueryupdate: configuration.apiServer.PES_URL + '/personnel-field/update', // 人员信息扩展修改
    Fieldqueryactive: configuration.apiServer.PES_URL + '/personnel-field/active',
    Fieldquerydelete: configuration.apiServer.PES_URL + '/personnel-field/delete',
    queryOrgList: configuration.apiServer.PES_URL + '/organization/queryOrgList', // 组织机构
    queryDetail: configuration.apiServer.PES_URL + '/organization/queryDetail', // 组织机构详情查询
    querySave: configuration.apiServer.PES_URL + '/organization/save', // 组织机构新增
    queryUpdate: configuration.apiServer.PES_URL + '/organization/update', // 组织机构编辑
    queryDelete: configuration.apiServer.PES_URL + '/organization/delete', // 组织机构删除
    postInfoQueryPage: configuration.apiServer.PES_URL + '/post-info/queryPage', // 岗位管理列表
    postInfosave: configuration.apiServer.PES_URL + '/post-info/save',
    postInfoqueryDetail: configuration.apiServer.PES_URL + '/post-info/queryDetail',
    postInfoupdate: configuration.apiServer.PES_URL + '/post-info/update',
    postInfodelete: configuration.apiServer.PES_URL + '/post-info/delete',
    importOrg: configuration.apiServer.PES_URL + '/organization/import',
    downloadTemplate: configuration.apiServer.PES_URL + '/organization/downloadTemplate',
    TeamqueryPage: configuration.apiServer.PES_URL + '/team-manage/queryPage',
    personnleQueryPage: configuration.apiServer.PES_URL + '/personnel-info/queryPage',
    Teamsave: configuration.apiServer.PES_URL + '/team-manage/save',
    TeamqueryDetail: configuration.apiServer.PES_URL + '/team-manage/queryDetail',
    TeamqueryUpdate: configuration.apiServer.PES_URL + '/team-manage/update',
    Teamquerydelete: configuration.apiServer.PES_URL + '/team-manage/delete',
    shiftOrderPage: configuration.apiServer.PES_URL + '/shift-order/page',
    addshiftOrder: configuration.apiServer.PES_URL + '/shift-order',
    deleteshiftOrder: configuration.apiServer.PES_URL + '/shift-order/delete',
    updateshiftOrder: configuration.apiServer.PES_URL + '/shift-order/update',
    addAccount: configuration.apiServer.PES_URL + '/personnel-info/addAccount',
    personnelexcel: configuration.apiServer.PES_URL + '/personnel-info/excel/template',
    personnelimportExcel: configuration.apiServer.PES_URL + '/personnel-info/importExcel',
    downloadExcel: configuration.apiServer.PES_URL + '/personnel-info/excel/downloadExcel',
    batchDelete: configuration.apiServer.PES_URL + '/personnel-info/batchDelete',
    batchDisable: configuration.apiServer.PES_URL + '/personnel-info/batchDisable',
    batchResetPassword: configuration.apiServer.PES_URL + '/personnel-info/batchResetPassword',
    queryPersonnelDetail: configuration.apiServer.PES_URL + '/personnel-info/queryPersonnelDetail',
    updateAccount: configuration.apiServer.PES_URL + '/personnel-info/updateAccount',
    updatePersonnelInfo: configuration.apiServer.PES_URL + '/personnel-info/updatePersonnelInfo',
    queryExportPage: configuration.apiServer.PES_URL + '/personnel-info/queryExportPage',
    iamUserInfo: configuration.apiServer.KEYCLOAK_URL + '/role/list',
    getClients: configuration.apiServer.KEYCLOAK_URL + '/sysApp/list',
    attributes: configuration.apiServer.KEYCLOAK_URL + '/users/systemRole',
    getUserById: configuration.apiServer.KEYCLOAK_URL + '/users/getUserById',
    usergroup: configuration.apiServer.KEYCLOAK_URL + '/sysUsergroup/list',
    updateUser: configuration.apiServer.KEYCLOAK_URL + '/sysUsergroup/{id}/adduser',
    deleteUser: configuration.apiServer.KEYCLOAK_URL + '/sysUsergroup/',
    uploadDispatch: configuration.apiServer.PES_URL + '/task-user/dispatch', // 任务调度
    teamQueryPersonnelList: configuration.apiServer.PES_URL + '/team-manage/queryPersonnelList', // 通过班组获取人员
    postInfoQyeryPage: configuration.apiServer.PES_URL + '/post-info/queryPage', // 获取岗位
    queryPersonnelList: configuration.apiServer.PES_URL + '/organization/queryPersonnelList',
    postInfoQyeryList: configuration.apiServer.PES_URL + '/post-info/queryPersonnelList', // 通过岗位获取人员
    reakMroleList: configuration.apiServer.PES_URL + '/system/realmrole/list',
    reakMroleUser: configuration.apiServer.PES_URL + '/system/realmrole/users',
    appRoleList: configuration.apiServer.PES_URL + '/app-role/list', // 应用领域角色关系表
    resourceList: configuration.apiServer.PES_URL + '/system/resource/list',
    sysAppList: configuration.apiServer.PES_URL + '/system/sysApp/list', // 应用列表查询
    addAppRole: configuration.apiServer.PES_URL + '/app-role', // 创建
    realmroleConfig: configuration.apiServer.PES_URL + '/system/realmrole/configRes', // 配置领域角色资源
    serchRealmrole: configuration.apiServer.PES_URL + '/system/realmrole', // 根据id查询领域角色信息
    sysaddUser: configuration.apiServer.PES_URL + '/system/realmrole/configusers', // 角色添加用户
    sysdelUser: configuration.apiServer.PES_URL + '/system/realmRole/delUser', // 角色删除用户
    getUserGroup: configuration.apiServer.PES_URL + '/user-group-type',
    addUserGroupType: configuration.apiServer.PES_URL + '/user-group-type/group',
    UserGroupename: configuration.apiServer.PES_URL + '/user-group-type/rename',
    UserGroupeRef: configuration.apiServer.PES_URL + '/user-group-ref',
    TaskGroupList: configuration.apiServer.TASK_URL + '/task-group/list', // 列表
    batchConfig: configuration.apiServer.TASK_URL + '/task-type-user/batchConfig', // 人员授权-任务授权
    savePersonnelModify: configuration.apiServer.PES_URL + '/personnel-modify-log/savePersonnelModify', // 保存人员变动
    PersonnelqueryPage: configuration.apiServer.PES_URL + '/personnel-modify-log/queryPage',
    queryTaskTypePage: configuration.apiServer.TASK_URL + '/task-type/queryPageWithUser', // 分页查询任务类型
    workRoom: configuration.apiServer.BASEINFO_URL + '/workroom/queryPage', // 查询厂房
    workArea: configuration.apiServer.BASEINFO_URL + '/work-area/queryPage', // 查询工区
    workShop: configuration.apiServer.BASEINFO_URL + '/workshop/queryPage', // 查询车间
    personTaskList: configuration.apiServer.PES_URL + '/task/task-type/user', // 根据人员id查询人员你任务权限列表
    swichOn: configuration.apiServer.PES_URL + '/task/task-type-user/switch/on', // 员工管理-授权信息-任务管理处理按钮
    swichOff: configuration.apiServer.PES_URL + '/task/task-type-user/switch/off', // switchOff
    taskTypeRemove: configuration.apiServer.PES_URL + '/task/task-type-user/remove',
    taskTypeAdd: configuration.apiServer.PES_URL + '/task/task-type-user/user/realm', // 新增任务
    queryDownOrgPersonnelList: configuration.apiServer.PES_URL + '/organization/queryDownOrgPersonnelList',
    addUserQuery: configuration.apiServer.PES_URL + '/system/realmRole/addUser', // 角色添加用户
    resourceUser: configuration.apiServer.PES_URL + '/system/resource/user',
    realmGroup: configuration.apiServer.PES_URL + '/system/realmrole/list/group',
    getUserRealmRoleList: configuration.apiServer.PES_URL + '/system/realmrole/getUserRealmRoleList',
    UserGroupTypeList: configuration.apiServer.PES_URL + '/user-group-type/list',
    originResouce: configuration.apiServer.PES_URL + '/system/resource/origin',
    addTeamType: configuration.apiServer.PES_URL + '/team-type', // 组分类新增
    TeamTypeList: configuration.apiServer.PES_URL + '/team-type/list', // 组分类列表查询
    updateTypeList: configuration.apiServer.PES_URL + '/team-type/update', // 组分类修改
    DeleteTypeList: configuration.apiServer.PES_URL + '/team-type', // 组分类删除
    taskMonitor: configuration.apiServer.PES_URL + '/task/monitor', // 任务监控查询
    taskMonitorLast: configuration.apiServer.PES_URL + '/task/monitor/last',
    taskInfoUser: configuration.apiServer.TASK_URL + '/task-info/query/user',
    updatePwd: configuration.apiServer.KEYCLOAK_URL + '/ldap/updatePwd'
  }
}

