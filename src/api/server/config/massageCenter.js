export default (configuration) => {
  return {
    mylist: configuration.apiServer.MESSAGE_CENTER + '/siteInbox/mylist', // 收件箱-我的消息
    listByUser: configuration.apiServer.KEYCLOAK_URL + '/sysApp/listByUser', // 获取应用
    getDic: configuration.apiServer.SYS_URL + '/base/dict/findByCode', // 数据字典
    readMessage: configuration.apiServer.MESSAGE_CENTER + '/siteInbox/read', // 阅读收件
    tIndexList: configuration.apiServer.INDEX + '/t-index-plate/list', // 综合搜索接口
    queryMySearchList: configuration.apiServer.CARD_URL + '/card-user-search-log/queryMySearchList',
    cleanMySearchList: configuration.apiServer.CARD_URL + '/card-user-search-log/clean',
    queryComprehensiveCardByPage: configuration.apiServer.CARD_URL + '/card-info/queryComprehensiveCardByPage',
    cardsubscribe: configuration.apiServer.CARD_URL + '/card-user/subscribe',
    cardunSubscribe: configuration.apiServer.CARD_URL + '/card-user/unSubscribe'
  }
}

