import request from '@/utils/request'
const kc = FRONT_CONFIG.keycloakConfig.rootUrl

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(params) {
  return request({
    url: '/users/info',
    method: 'get',
    params
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function postChangePassword(data, header) {
  return request({
    url: '/ldap/updatePwd',
    method: 'put',
    data,
    header
  })
}

// 获取用户信息
export function getUserInfo({ realm, username }) {
  return request({
    url: kc + 'v3/users/info',
    method: 'get',
    params: {
      realm,
      username
    }
  })
}

// 设置当前公司
export function setCompany(orgId) {
  return request({
    url: kc + 'v3/users/changeEnterprise',
    method: 'post',
    data: {
      orgId
    }
  })
}

// 获取用户有的组织
export function getUserOrgs(orgId) {
  return request({
    url: kc + `sysOrganization/userOrg/${orgId}`,
    method: 'get'
  })
}

// 获取当前公司下所有组织
export function getOrgs(orgId) {
  return request({
    url: kc + `sysOrganization/downTree/${orgId}`,
    method: 'get'
  })
}

// 获取全部组织
export function getAllOrganization(orgId) {
  return request({
    url: `${kc}sysOrganization/tree`,
    method: 'get'
  })
}
