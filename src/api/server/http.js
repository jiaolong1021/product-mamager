import Ajax from '@zlst/ajax'
import { API_LIST } from './config/index.js'

Ajax.init({
  apiList: API_LIST
})

export default Ajax.methods
