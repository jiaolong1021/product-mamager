import request from '@/utils/request'

// 获取卡片分组
export function getGroups(data) {
  return request({
    url: '/crad-group/page',
    method: 'post',
    data
  })
}
// 创建卡片分组
export function addGroup(data) {
  return request({
    url: '/crad-group',
    method: 'post',
    data
  })
}

// 重命名分组
export function renameGroup(data) {
  return request({
    url: '/crad-group/rename',
    method: 'post',
    data
  })
}

// 获取分组信息和子信息
export function getGroupInfo(params) {
  return request({
    url: '/crad-group/tree',
    method: 'post',
    params
  })
}

// 删除分组
export function deleteGroup(params) {
  return request({
    url: '/crad-group/delete',
    method: 'post',
    params
  })
}

