import request from '@/utils/request'

// 新建卡片
export function addCard(data) {
  return request({
    url: '/card-info/saveCardInfo',
    method: 'post',
    data
  })
}

// 获取授权卡片
export function getAuthCard() {
  return request({
    url: '/card-info/allowList',
    method: 'get'
  })
}

// 删除卡片
export function deleteCard(cardId) {
  return request({
    url: `/card-info/deleteCardInfo/${cardId}`,
    method: 'delete'
  })
}

// 查询卡片详情
export function getCardInfo(cardId) {
  return request({
    url: `/card-info/queryCardInfoDetail/${cardId}`,
    method: 'get'
  })
}
// 获取卡片列表
export function getCardList(data) {
  return request({
    url: '/card-info/queryCardInfoPage',
    method: 'post',
    data
  })
}
// 获取子集卡片
export function getChildCard(cardParentId) {
  return request({
    url: `/card-info/queryListByParentId/${cardParentId}`,
    method: 'post'
  })
}
// 启用/禁用卡片
export function updateCardCondition(data) {
  return request({
    url: '/card-info/updateCardActive',
    method: 'post',
    data
  })
}
// 编辑卡片权限
export function authCard(data) {
  return request({
    url: '/card-info/updateCardAuthority',
    method: 'post',
    data
  })
}
// 编辑卡片
export function editCard(data) {
  return request({
    url: '/card-info/updateCardInfo ',
    method: 'post',
    data
  })
}

// 删除容器节点
export function deleteContainerNode(id) {
  return request({
    url: `/card-container/delete/${id}`,
    method: 'delete'
  })
}

// 获取卡片容器树
export function getContainerTree(data) {
  return request({
    url: `/card-container/getTree`,
    method: 'post',
    data
  })
}

// 查询当前容器所有卡片
export function getContainerCard(data) {
  return request({
    url: `/card-container/queryCardContainerList`,
    method: 'post',
    data
  })
}
// 添加看片容器节点
export function saveContainerCard(data) {
  return request({
    url: `/card-container/save`,
    method: 'post',
    data
  })
}

// 更新绑定的容器
export function updateContainerCard(data) {
  return request({
    url: `/card-container/update`,
    method: 'post',
    data
  })
}
