import { createWebSocket } from '@/utils/heartBeat'
// eslint-disable-next-line no-undef
const systemUrl = FRONT_CONFIG.apiServer.WS_URL

export function getWsConnect(params) {
  createWebSocket(`${systemUrl}/ws/iam/${params.realm}/${params.info}`)
}
