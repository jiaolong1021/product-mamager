import request from '@/utils/request'

request.defaults.baseURL = FRONT_CONFIG.apiServer.KEYCLOAK_URL

// 修改密码
export function changePassword(data, header) {
  return request({
    url: '/ldap/updatePwd',
    method: 'put',
    data,
    header
  })
}

// 初始化密码
export function initPassword(data, header) {
  return request({
    url: '/ldap/changeInitPassword',
    method: 'put',
    data,
    header
  })
}

// 密码策略
export function getPolicy(params) {
  return request({
    url: `/sysSecurityPolicy/realm/${params}`,
    method: 'get'
  })
}

// 获取用户密码策略
export function getUserPolicy(userId) {
  return request({
    url: `/sysSecurityPolicy/user/${userId}`,
    method: 'get'
  })
}

