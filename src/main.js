import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui-rw-dispatcher/styles/index.scss'
import './assets/font/iconfont.css'
import '@/assets/styles/index.scss'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n
import prototypeExtendObj from './utils/prototype/prototypeExtend.js' // 扩展原生对象
import App from './App'
import store from './store'
import router from './router'
import '@/icons' // icon
import '@/permission' // 权限控制
import '@/utils/element.js' // vue组件按需调用
import '@/assets/icons/font_icon/iconfont.css'
import { configuration } from '@/store/global'
import zlstElement from '@/layout/zlst'
import $http from './api/server/http'
import { subject } from './registerApps'
import { cloneDeep } from 'lodash'
import '@zl/product/lib/theme-default/index.css'
import { zl } from '@zl/product'

Vue.prototype.$ELEMENT = { size: 'small', ...locale }
Vue.prototype.$http = $http
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false

prototypeExtendObj.init()

// 注册权限指令
Vue.directive('sys-resource', {
  bind: function(el, binding, vnode) {
    const resourceName = binding.arg
    if (!store.state.sys.authList.includes(resourceName)) {
      el.className += ' zlst-display-none'
    }
  },
  inserted: function(el, binding, vnode) {},
  update: function(el, binding, vnode) {
    const resourceName = binding.arg
    if (!store.state.sys.authList.includes(resourceName)) {
      el.className += ' zlst-display-none'
    }
  },
  componentUpdated: function(el, binding, vnode) {},
  unbind: function(el, binding, vnode) {}
})

var rawGetComputedStyle = window.getComputedStyle
window.getComputedStyle = function(el, pseudoElt) {
  if (el === document) return { overflow: 'auto' }
  if (el instanceof HTMLElement) {
    return rawGetComputedStyle(el, pseudoElt)
  } else {
    return {}
  }
}
// 解决定位问题
Object.defineProperty(document, 'scrollLeft', {
  get() {
    return document.documentElement.scrollLeft
  }
})
Object.defineProperty(document, 'scrollTop', {
  get() {
    return document.documentElement.scrollTop
  }
})
Vue.prototype
  .$keycloak(configuration.baseUrl.keycloakConfig)
  .then(async kc => {
    // kc为通过keycloak登录后返回的用户信息，在所有页面都可以通过 this.$userInfo 来获取
    localStorage.setItem('token', Vue.prototype.$userInfo.token)
    localStorage.setItem('username', Vue.prototype.$userInfo.userRep.username)
    localStorage.setItem('realm', Vue.prototype.$userInfo.realm)
    // 通过clientid解决跳转不同系统，菜单不加载问题
    const preClientId = localStorage.getItem('clientId')
    const clientId = Vue.prototype.$userInfo.clientId
    if (preClientId && preClientId !== clientId) {
      store.commit('sys/SET_MENU', [])
      store.commit('sys/SET_PATH_LIST', [])
    }
    store.commit('sys/SET_REG_STATUE', false)
    if (Vue.prototype.$userInfo.userRep.accessEnterprise) {
      if (
        !store.state.sys.currentCompany ||
        store.state.sys.currentCompany.id !==
          Vue.prototype.$userInfo.userRep.accessEnterprise.id
      ) {
        store.commit(
          'sys/SET_CURRENT_ORG',
          Vue.prototype.$userInfo.userRep.accessEnterprise
        )
      }
    } else {
      if (
        Vue.prototype.$userInfo.userRep.enterprise &&
        Vue.prototype.$userInfo.userRep.enterprise.length > 0
      ) {
        store.dispatch(
          'sys/setCompany',
          Vue.prototype.$userInfo.userRep.enterprise[0]
        )
      }
    }
    store.dispatch('sys/getEnter')
    await store.dispatch('sys/getCompanyOrg')
    Vue.use(zlstElement)
    zl.init(Vue, {
      url: {
        workbench: FRONT_CONFIG.apiServer.BASE_URL, // 工作台
        index: FRONT_CONFIG.apiServer.INDEX, // 指标
        kc: FRONT_CONFIG.keycloakConfig.rootUrl // keycloak
      }
    })
    // Vue.use(ZlProduct, {
    //   url: {
    //     workbench: FRONT_CONFIG.apiServer.BASE_URL, // 工作台
    //     index: FRONT_CONFIG.apiServer.INDEX, // 指标
    //     kc: FRONT_CONFIG.keycloakConfig.rootUrl // keycloak
    //   }
    // })
    const instance = new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
    instance.$watch('$keycloak.token', n => {
      Vue.prototype.$userInfo.token = n
      localStorage.setItem('token', n)
      // 当token变化时需要通知子应用的token
      subject.next({
        type: 'parent',
        payload: {
          kc: cloneDeep(Vue.prototype.$keycloak),
          userInfo: cloneDeep(Vue.prototype.$userInfo)
        }
      })
    })
  })
  .catch(err => {
    console.error(err)
  })
