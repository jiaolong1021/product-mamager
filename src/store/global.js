import { cloneDeep } from 'lodash'

export const configuration = {
  /* eslint-disable no-undef */
  baseUrl: cloneDeep(FRONT_CONFIG)
}
