import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import getters from './getters'
import dictionary from './modules/dictionary'
import app from './modules/app'
import settings from './modules/settings'
import { configuration } from './global'
import orgData from './modules/org-data' // 组织结构
import sys from './modules/sys' // 系统管理
import user from './modules/user' // 用户信息
import messageCenter from './modules/messageCenter'

// 业务modules
import personnelManager from './modules/personnelManager'
import taskConfig from './modules/taskConfig'

Vue.use(Vuex)

let store = null
// 注意: 乾坤环境中app，user是由基座传下来的，不应该在本地有额外改动！

store = new Vuex.Store({
  modules: {
    dictionary,
    orgData,
    sys,
    user,
    app,
    settings,
    taskConfig,
    personnelManager,
    messageCenter
  },
  getters,
  plugins: [new VuexPersistence().plugin]
})

store.replaceState(Object.assign({}, store.state, { configuration }))

export const modules = {
  app,
  settings,
  sys,
  user
}

export default store
