import defaultSettings from '@/settings'

const { showSettings, fixedHeader, sidebarLogo, sidebarMode } = defaultSettings

const state = {
  queryStatus: 'done',
  editStatus: {},
  showSettings: showSettings,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  sidebarMode: sidebarMode
}

const mutations = {
  QUERY_STATUS: (state, data) => {
    state.queryStatus = data
  },
  CHANGE_SETTING: (state, { key, value }) => {
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  },
  EDIT_STATUS: (state, data) => {
    state.editStatus[data.type] = {
      title: data.title,
      status: data.status
    }
  }
}

const actions = {
  changeSetting({ commit, state }, data) {
    commit('CHANGE_SETTING', data)
  },
  setOperate({ commit, state }, data) {
    commit('EDIT_STATUS', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

