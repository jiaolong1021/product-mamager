import $http from '@/api/server/http'
import { Message } from 'element-ui'
import Vue from 'vue'
const _handleError = message => {
  Message({
    message,
    type: 'error'
  })
  return false
}
const sysConfig = () => {
  const userInfo = Vue.prototype.$userInfo
  const config = {
    headers: {
      Authorization: 'Bearer ' + userInfo.token,
      clientId: userInfo.clientId,
      realm: userInfo.realm,
      token: ''
    }
  }
  return config
}
const actions = {
  // 分页获取班制
  getShiftsList({ commit }, param) {
    const config = sysConfig()
    return $http.post('getShiftsList', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 新增班制
  addShiftItem({ commit }, param) {
    const config = sysConfig()
    return $http.post('addShiftItem', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 更新
  updateShiftItme({ commit }, param) {
    const config = sysConfig()
    return $http.post('updateShiftItme', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 删除班制
  deleteShift({ commit }, id) {
    const config = sysConfig()
    return $http.post('deleteShift', {}, { ...config }, id).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 规则分页列表
  getShiftRulesList({ commit }, param) {
    const config = sysConfig()
    return $http.post('getShiftRulesList', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 规则详情
  shiftRulesInfo({ commit }, id) {
    const config = sysConfig()
    return $http.get('addShiftRules', { ...config, params: {}}, id).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 新增规则
  addShiftRules({ commit }, param) {
    const config = sysConfig()
    return $http.post('addShiftRules', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 修改规则
  updateShiftRules({ commit }, param) {
    const config = sysConfig()
    return $http.post('updateShiftRules', param, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  },
  // 删除规则
  deleteShiftRules({ commit }, id) {
    const config = sysConfig()
    return $http.post('deleteShiftRules', {}, { ...config }, id).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    })
  }
}
export default {
  namespaced: true,
  actions
}
