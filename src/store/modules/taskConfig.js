import $http from '@/api/server/http'
import { Message } from 'element-ui'
import Vue from 'vue'
const _handleError = message => {
  Message({
    message,
    type: 'error'
  })
  return false
}

const state = {
  queryStatus: 'done'
}
const mutations = {
  QUERY_STATUS: (state, data) => {
    state.queryStatus = data
  }
}

const actions = {
  // 任务配置查询
  queryTaskconfig({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
        // realm: userInfo.realm,
        // clientId: userInfo.clientId
      }
    }
    return $http.get('getConfigList', { params: {}, ...config }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 创建成员
  saveMember({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('saveMember', params, { headers: {
      Authorization: 'Bearer ' + userInfo.token
    }}).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 删除成员
  delMember({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.delete('delMember', {}, { headers: {
      Authorization: 'Bearer ' + userInfo.token
    }}, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
