import $http from '@/api/server/http'
const state = {
  queryStatus: 'done',
  data: [],
  status: 'null'
}

const mutations = {
  QUERY_STATUS: (state, data) => {
    state.queryStatus = data
  },
  SET_ORG: (state, data) => {
    state.data = data
  },
  SET_STATUS: (state, status) => {
    state.status = status
  }
}

const actions = {
  getOrg({ state, commit }, args) {
    if (state.status === 'success' || state.status === 'pending') {
      return Promise.resolve()
    }
    commit('SET_STATUS', 'pending')
    return $http.getRestful(...args)
      .then(res => {
        if (res.data.code !== 0) {
          commit('SET_STATUS', 'error')
          return []
        } else {
          commit('SET_ORG', res.data.data)
          commit('SET_STATUS', 'success')
          return res.data.data
        }
      })
      .catch(err => {
        commit('SET_STATUS', 'error')
        console.log(err)
      })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
