import $http from '@/api/server/http'
import { Message } from 'element-ui'
import Vue from 'vue'
const _handleError = message => {
  Message({
    message,
    type: 'error'
  })
  return false
}
const sysConfig = () => {
  const userInfo = Vue.prototype.$userInfo
  const config = {
    headers: {
      Authorization: 'Bearer ' + userInfo.token,
      clientId: userInfo.clientId,
      realm: userInfo.realm,
      token: ''
    }
  }
  return config
}

const state = {
  queryStatus: 'done',
  sampleList: [], // 样品清单
  groupList: [], // 组样登记
  groupInfo: {}, // 单个组样详情
  smpleInfo: {}
}
const mutations = {
  QUERY_STATUS: (state, data) => {
    state.queryStatus = data
  },
  SET_SAMPLE_LIST: (state, data) => {
    state.sampleList = data
  },
  SET_GROUP_LIST: (state, data) => {
    state.groupList = data
  },
  SET_GROUP_INFO: (state, data) => {
    state.groupInfo = data
  },
  SET_SMPLE_INFO: (state, data) => {
    state.smpleInfo = data
  }
}

const actions = {
  // 查询应用信息列表
  queryAppInfoList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('queryAppInfoList', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 分页查询任务类型
  queryTaskTypePage({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('queryTaskTypePage', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 创建应用
  saveAppInfo({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('saveAppInfo', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 删除应用
  deleteAppInfo({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.delete('deleteAppInfo', {}, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 启用或禁用任务类型
  updateTaskTypeActive({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('updateTaskTypeActive', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 编辑任务
  updateTaskType({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('updateTaskType', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 删除注册任务
  deleteTaskType({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.delete('deleteTaskType', {}, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 新增注册任务
  saveTaskType({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('saveTaskType', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 任务树
  taskTypeTree({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('taskTypeTree', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 组织机构
  queryOrgList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('queryOrgList', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 任务配置查询
  taskInfoList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('taskInfoList', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // iam获取角色树
  iamUserInfo({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }
    return $http.get('iamUserInfo', { params: params, ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  getClients({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }
    return $http.post('getClients', params, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  iamUserList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }
    return $http.post('iamUserList', params, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 根据组织查询人员
  queryPersonnelList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
        // realm: userInfo.realm,
        // clientId: userInfo.clientId
      }
    }
    return $http.get('queryPersonnelList', { params: {}, ...config }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 任务调度
  uploadDispatch({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }
    return $http.post('uploadDispatch', params, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 人员中心班组
  TeamqueryPage({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('TeamqueryPage', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  teamQueryPersonnelList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
        // realm: userInfo.realm,
        // clientId: userInfo.clientId
      }
    }
    return $http.get('teamQueryPersonnelList', { params: {}, ...config }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  postInfoQyeryPage({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('postInfoQyeryPage', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  postInfoQyeryList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
        // realm: userInfo.realm,
        // clientId: userInfo.clientId
      }
    }
    return $http.get('postInfoQyeryList', { params: {}, ...config }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  updatePwd({ commit }, params) {
    const config = sysConfig()
    return $http.put('updatePwd', params, { ...config }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
