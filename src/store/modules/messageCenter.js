import $http from '@/api/server/http'
import { Message } from 'element-ui'
import Vue from 'vue'
const _handleError = message => {
  Message({
    message,
    type: 'error'
  })
  return false
}
const state = {
  queryStatus: 'done',
  sampleList: [], // 样品清单
  groupList: [], // 组样登记
  dicInfo: [],
  sysAppInfo: {}
}
const mutations = {
  QUERY_STATUS: (state, data) => {
    state.queryStatus = data
  },
  SET_SAMPLE_LIST: (state, data) => {
    state.sampleList = data
  },
  SET_GROUP_LIST: (state, data) => {
    state.groupList = data
  },
  SET_DIC_INFO: (state, data) => {
    state.dicInfo = data
  },
  SET_SYSAPP_INFO: (state, data) => {
    state.sysAppInfo = data
  }
}

const actions = {
  // 查询应用信息列表
  mylist({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('mylist', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        // return _handleError(res.data.message)
        return
      }
      return res
    }).catch(function(error) {
      console.log(error)
      // return _handleError(error.response.data.message)
    })
  },
  // 获取应用
  listByUser({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.get('listByUser', {
      params: {
        realm: userInfo.realm
      },
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      commit('SET_SYSAPP_INFO', res.data.data)
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  getDic({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.get('getDic', {
      params: { systemCode: 'MESSAGE_CENTER', typeCode: 'MSG_LEVEL' },
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        realm: userInfo.realm,
        clientId: userInfo.clientId
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      commit('SET_DIC_INFO', res.data.data)
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  // 阅读收件
  readMessage({ commit, state }, data) {
    if (state.queryStatus === 'pedding') {
      return Promise.resolve()
    }
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token,
        token: userInfo.token,
        user_id: userInfo.userRep.id
      }
    }
    return $http.put('readMessage', data, config, data).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.resultMsg)
      }
      return res
    })
  },
  tIndexList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    const config = {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
        // realm: userInfo.realm,
        // clientId: userInfo.clientId
      }
    }
    return $http.get('tIndexList', { params: {}, ...config }, params).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  queryMySearchList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('queryMySearchList', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  cleanMySearchList({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('cleanMySearchList', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  queryComprehensiveCardByPage({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('queryComprehensiveCardByPage', params, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  cardsubscribe({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('cardsubscribe', {}, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }, params
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  },
  cardunSubscribe({ state, commit }, params) {
    const userInfo = Vue.prototype.$userInfo
    return $http.post('cardunSubscribe', {}, {
      headers: {
        Authorization: 'Bearer ' + userInfo.token
      }, params
    }).then(res => {
      if (res.data.code !== '0') {
        return _handleError(res.data.message)
      }
      return res
    }).catch(function(error) {
      return _handleError(error.response.data.message)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
