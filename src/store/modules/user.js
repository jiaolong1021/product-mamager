import { getToken } from '@/utils/auth'
const state = {
  token: getToken(),
  psdReg: {} // 密码策略
}
const mutations = {
  SET_PSD_REG: (state, data) => {
    state.psdReg = data
  }
}
const actions = {
  setPsdReg({ commit }, data) {
    commit('SET_PSD_REG', data)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

