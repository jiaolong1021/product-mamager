## 中铝视拓生产管控微前端基座

git地址：http://git.cs2025.com/front/product-manager.git <br>
jenkins: http://jenkins.cs2025.com/job/chalco-pcs-main-web/


> 子应用
#### 模块： 通用功能
git地址：http://git.cs2025.com/front/chalco-pcs-common-web.git

模块： 质量模块
git地址：http://git.cs2025.com/front/chalco-pcs-quality-web

模块： 调度管理
git地址：http://git.cs2025.com/front/chalco-pcs-scheduler-web.git  

模块： 首页
git地址：git@git.cs2025.com:front/product-manager-index.git
jenkins: http://jenkins.cs2025.com/job/chalco-pcs-index-web/

模块： 计划模块
git地址：http://git.cs2025.com/front/chalco-pcs-plan-web

模块： 系统管理
git地址：http://git.cs2025.com/front/chalco-pcs-manager-web.git

模块： 指标管理
git地址：http://git.cs2025.com/front/chalco-pcs-target-management-web.git

git@git.cs2025.com:front/product-manager-sys.git
http://jenkins.cs2025.com/job/chalco-pcs-workbench-web/