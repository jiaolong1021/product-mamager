module.exports = {
  presets: [
    ['@vue/app', {
      useBuiltIns: 'usage'
    }
    ]
  ],
  sourceType: 'unambiguous',
  'plugins': [
    ['import', { libraryName: '@zl/product', libraryDirectory: 'lib' }, '@zl/product'],
    ['import', { libraryName: 'lodash', libraryDirectory: '', camel2DashComponentName: false }, 'lodash']
  ]
}
